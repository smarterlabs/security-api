from flask import Flask, jsonify, request
from flask_cors import CORS
from datetime import datetime
from flask_bcrypt import Bcrypt
import jwt
import datetime
from functools import wraps
import json


app = Flask(__name__)
CORS(app)

app.config['JWT_SECRET_KEY'] = 'test'
bcryptApp = Bcrypt(app)




@app.route('/')
def test2():
    return "hello"


@app.route('/users/login', methods=['POST', "GET"])
def login():
    email = request.get_json()['email']
    password = request.get_json()['password']

    result = ""

    # Search in db
    response = True

    if response:
        if bcryptApp.check_password_hash("$2b$12$uzR1dCVLcxpLybxRo5jFaesUvpnjPuh41TCP4ioZNB4Q95su73iyi", password):
            access_token = jwt.encode({
                'first_name': "v",  # response['first_name'],
                'last_name': "i",  # response['last_name'],
                'email': email,
                'exp': datetime.datetime.utcnow() + datetime.timedelta(hours = 2)}, app.config["JWT_SECRET_KEY"])

            result = jsonify({'token': access_token})
        else:
            result = jsonify({"error": "Invalid username and password"})
    else:
        result = jsonify({"error": "No results found"})
    return result


def token_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        token = request.get_json()['token']
        if not token:
            return jsonify({"redirect": "token is null"})

        try:
            jwt_data = jwt.decode(token, app.config["JWT_SECRET_KEY"], algorithms=["HS256"])
        except jwt.ExpiredSignatureError:
            return {"redirect": "invalide token"}
        except Exception:
            return {"redirect": "invalide token"}
        return f(*args, **kwargs)

    return decorated_function


@app.route('/inputs_data', methods=['POST'])
@token_required
def inputs_data():
        return {'dskadk':'daskdkas'}


# app.run()
